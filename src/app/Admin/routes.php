<?php

use Illuminate\Support\Facades\Auth;

Route::get('', ['as' => 'admin.dashboard', function () {
    $name = Auth::user()->name;
    $content = "Hello, $name!";
    return AdminSection::view($content, 'Dashboard');
}]);
