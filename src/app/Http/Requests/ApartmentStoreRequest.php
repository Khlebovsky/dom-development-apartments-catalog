<?php

namespace App\Http\Requests;

class ApartmentStoreRequest extends ApiFormRequest
{
    /** @inheritdoc */
    public function authorize(): bool
    {
        return true;
    }

    /** @inheritdoc */
    public function rules(): array
    {
        return [
            'video'  => ['required'],
            'layout' => ['nullable'],
            'type'   => ['required'],
            'area'   => ['numeric'],
            'price'  => ['numeric'],
        ];
    }
}
