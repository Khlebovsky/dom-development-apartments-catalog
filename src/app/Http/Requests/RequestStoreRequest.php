<?php

namespace App\Http\Requests;

class RequestStoreRequest extends ApiFormRequest
{
    /** @inheritdoc */
    public function authorize(): bool
    {
        return true;
    }

    /** @inheritdoc */
    public function rules(): array
    {
        return [
            'name'      => ['required', 'min:4'],
            'phone'     => ['required'],
            'apartment' => ['nullable', 'integer']
        ];
    }
}
