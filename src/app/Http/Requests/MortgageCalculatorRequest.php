<?php

namespace App\Http\Requests;

class MortgageCalculatorRequest extends ApiFormRequest
{
    /** @inheritdoc */
    public function authorize(): bool
    {
        return true;
    }

    /** @inheritdoc */
    public function rules(): array
    {
        return [
            'initial_fee' => ['required', 'numeric'],
            'sum'         => ['required', 'numeric'],
            'percentage'  => ['required', 'numeric'],
            'years'       => ['required', 'integer'],
        ];
    }
}
