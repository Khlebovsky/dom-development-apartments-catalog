<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Section;

/**
 * Class Mortgages
 *
 * @property \App\Models\Mortgage $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Mortgage extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth(100)->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('name', 'Название')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::datetime('validity', 'Истекает')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('apartment', 'Квартира (id)')->setHtmlAttribute('class', 'text-center')
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::text('id', '#')->required()->setReadonly(true),
            AdminFormElement::html('<hr>'),

            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('name', 'Название')->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::number('min_deposit', 'Минимальный взнос (руб)')
                    ->setStep(0.01)
                    ->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::number('interest_rate', 'Ставка (%)')
                    ->setStep(0.01)
                    ->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::number('term', 'Срок (лет)')
                    ->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::date('validity', 'Когда предложение истекает')
                    ->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::select('apartment', 'Квартира (id)')
                    ->setModelForOptions(\App\Models\Apartment::class, 'id')
                    ->required(),
                AdminFormElement::html('<hr>'),
            ])]);

        $form->getButtons()->setButtons([
            'save'   => new Save(),
            'cancel' => new Cancel(),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
