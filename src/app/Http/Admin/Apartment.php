<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Section;

/**
 * Class Apartments
 *
 * @property \App\Models\Apartment $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Apartment extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth(100)->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('type', 'Тип')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::image('layout', 'Планировка')->setImageWidth(500),
            AdminColumn::text('area', 'Площадь')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('price', 'Цена')->setHtmlAttribute('class', 'text-center')
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', '#')->required()->setReadonly(true),
                AdminFormElement::html('<hr>'),

                AdminFormElement::text('video', 'Ссылка на видео')->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::image('layout', 'Планировка')->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::select('type', 'Тип')
                    ->setEnum(\App\Models\Apartment::$aTypes)
                    ->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::number('area', 'Площадь (м2)')
                    ->setStep(0.01)
                    ->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::number('price', 'Цена (руб)')
                    ->setStep(0.01)
                    ->required(),
                AdminFormElement::html('<hr>'),
            ])]);

        $form->getButtons()->setButtons([
            'save'   => new Save(),
            'cancel' => new Cancel(),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
