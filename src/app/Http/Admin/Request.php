<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Section;

/**
 * Class Requests
 *
 * @property \App\Models\Request $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Request extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('name', 'Имя')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('phone', 'Телефон')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('apartment', 'Квартира (id)')->setHtmlAttribute('class', 'text-center'),

        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', '#')->required()->setReadonly(true),
                AdminFormElement::html('<hr>'),

                AdminFormElement::text('name', 'Имя')->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::text('phone', 'Телефон')->required(),
                AdminFormElement::html('<hr>'),

                AdminFormElement::select('apartment', 'Квартира (id)')
                    ->setModelForOptions(\App\Models\Apartment::class, 'id')
                    ->required(),
                AdminFormElement::html('<hr>'),
            ])]);

        $form->getButtons()->setButtons([
            'save'   => new Save(),
            'cancel' => new Cancel(),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
