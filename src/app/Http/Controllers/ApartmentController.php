<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApartmentStoreRequest;
use App\Models\Apartment;
use Illuminate\Http\Response;

class ApartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): response
    {
        $oItems = Apartment::query()->orderBy('id')->get();

        return response(Apartment::prepareData($oItems));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ApartmentStoreRequest $request
     *
     * @return Response
     */
    public function store(ApartmentStoreRequest $request): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ApartmentStoreRequest $request
     * @param int $id
     *
     * @return Response
     */
    public function update(ApartmentStoreRequest $request, $id): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }
}
