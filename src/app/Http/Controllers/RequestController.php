<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestStoreRequest;
use App\Models\Request as RequestModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RequestStoreRequest $request
     *
     * @return Response
     */
    public function store(RequestStoreRequest $request): response
    {
        return response(RequestModel::query()->create($request->validationData()), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id): response
    {
        return response('This method is not supported by the API', Response::HTTP_NOT_IMPLEMENTED);
    }
}
