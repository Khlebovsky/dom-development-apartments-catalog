<?php

namespace App\Http\Controllers;

use App\Classes\Calculators\MortgageCalculator;
use App\Http\Requests\MortgageCalculatorRequest;
use Illuminate\Http\Response;

class CalculatorController extends Controller
{
    public function monthlyPayment(MortgageCalculatorRequest $request)
    {
        $fResult = MortgageCalculator::calculateMonthlyPayment(
            $request->get('initial_fee'),
            $request->get('sum'),
            $request->get('percentage'),
            $request->get('years')
        );
        $aResult = ['result' => $fResult];

        return response()->json($aResult, Response::HTTP_OK);
    }
}
