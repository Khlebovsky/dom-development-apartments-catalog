<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    use HasFactory;

    /** @var string[] типы квартир */
    public static array $aTypes = [
        'Cтудия',
        '1 комнатная',
        '2 комнатная',
        '2 комнатная (евро)',
        '3 комнатная',
        '3 комнатная (евро)',
        '4 комнатная (евро)',
    ];

    /**
     * @return array|null
     */
    public function getMortgage(): ?array
    {
        $aKeys = [
            'name',
            'min_deposit',
            'interest_rate',
            'term',
            'validity',
        ];
        return $this->hasOne(Mortgage::class, 'apartment')->first()?->only($aKeys);
    }

    /**
     * @param Collection $oItems
     *
     * @return Collection
     */
    public static function prepareData(Collection $oItems): Collection
    {
        /** @var Apartment $oItem */
        foreach ($oItems as $oItem) {
            $oItem->mortgage = $oItem->getMortgage();
        }

        return $oItems;
    }
}
