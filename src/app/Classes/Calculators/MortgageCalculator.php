<?php

namespace App\Classes\Calculators;

/**
 * Класс для рассчета ипотечных платежей
 */
class MortgageCalculator
{
    /**
     * Рассчитывает ежемесячный платеж по ипотеке в рублях
     *
     * @param float $fInitialFee первый взнос (руб)
     * @param float $fMortgageSum общая сумма ипотеки (без вычета первого взноса, руб)
     * @param float $fPercentage процентная ставка (годовая)
     * @param int $iYears срок ипотеки (в годах)
     *
     * @return int
     */
    public static function calculateMonthlyPayment(
        float $fInitialFee,
        float $fMortgageSum,
        float $fPercentage,
        int   $iYears
    ): int
    {
        $iMonths = $iYears * 12;
        $fMonthlyRate = $fPercentage / 12 / 100;
        $fTotalRate = (1 + $fMonthlyRate) ** $iMonths;

        $fResult = ($fMortgageSum - $fInitialFee) * $fMonthlyRate * $fTotalRate / ($fTotalRate - 1);

        return ceil($fResult);
    }
}
