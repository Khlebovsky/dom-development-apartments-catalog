<?php

namespace App\Providers;

use App\Models\Apartment;
use App\Models\Mortgage;
use App\Models\Request;
use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        Apartment::class => 'App\Http\Admin\Apartment',
        Mortgage::class  => 'App\Http\Admin\Mortgage',
        Request::class   => 'App\Http\Admin\Request'
    ];

    /**
     * Register sections.
     *
     * @param Admin $admin
     *
     * @return void
     */
    public function boot(Admin $admin): void
    {
        parent::boot($admin);
    }
}
