<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

/**
 * @extends Factory
 */
class ApartmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $aTypes = [
            'Cтудия',
            '1 комнатная',
            '2 комнатная',
            '2 комнатная (евро)',
            '3 комнатная',
            '3 комнатная (евро)',
            '4 комнатная (евро)',
        ];

        return [
            'video'  => $this->faker->url(),
            'layout' => $this->faker->filePath(),
            'type'   => Arr::random($aTypes),
            'area'   => $this->faker->numberBetween(10, 80),
            'price'  => $this->faker->randomFloat(1, 1000000, 10000000)
        ];
    }
}
