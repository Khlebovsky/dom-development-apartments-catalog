<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class MortgageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name'          => 'Новая программа',
            'min_deposit'   => $this->faker->randomFloat(1, 500000, 1500000),
            'interest_rate' => $this->faker->randomFloat(1, 5, 15),
            'term'          => 30,
            'validity'      => '2022-12-30'
        ];
    }
}
