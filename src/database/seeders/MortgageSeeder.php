<?php

namespace Database\Seeders;

use App\Models\Mortgage;
use Illuminate\Database\Seeder;

class MortgageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Mortgage::factory()
            ->count(5)
            ->create();
    }
}
