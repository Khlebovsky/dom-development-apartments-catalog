<?php

namespace Database\Seeders;

use App\Models\Apartment;
use App\Models\Mortgage;
use Illuminate\Database\Seeder;

class ApartmentsMortgagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $aData = [
            [
                'apartment' => [
                    'video'  => 'https://www.youtube.com/watch?v=FmCH6blxvP8',
                    'layout' => 'images/uploads/apartment1.png',
                    'type'   => '2 комнатная (евро)',
                    'area'   => 57.6,
                    'price'  => 6500000,
                ],
                'mortgage'  => [
                    'name'          => 'Новая программа',
                    'min_deposit'   => 1000000,
                    'interest_rate' => 5.49,
                    'term'          => 30,
                    'validity'      => '2022-12-30'
                ]
            ],
            [
                'apartment' => [
                    'video'  => 'https://www.youtube.com/watch?v=FmCH6blxvP8',
                    'layout' => 'images/uploads/apartment2.png',
                    'type'   => '2 комнатная (евро)',
                    'area'   => 57.6,
                    'price'  => 6500000,
                ],
                'mortgage'  => [
                    'name'          => 'Новая программа',
                    'min_deposit'   => 1000000,
                    'interest_rate' => 5.49,
                    'term'          => 30,
                    'validity'      => '2022-12-30'
                ]
            ],
            [
                'apartment' => [
                    'video'  => 'https://www.youtube.com/watch?v=FmCH6blxvP8',
                    'layout' => 'images/uploads/apartment3.png',
                    'type'   => '3 комнатная (евро)',
                    'area'   => 92.8,
                    'price'  => 9500000,
                ],
                'mortgage'  => [
                    'name'          => 'Новая программа',
                    'min_deposit'   => 1000000,
                    'interest_rate' => 5.49,
                    'term'          => 30,
                    'validity'      => '2022-12-30'
                ]
            ],
            [
                'apartment' => [
                    'video'  => 'https://www.youtube.com/watch?v=FmCH6blxvP8',
                    'layout' => 'images/uploads/apartment4.png',
                    'type'   => 'Cтудия',
                    'area'   => 27.6,
                    'price'  => 2500000,
                ],
                'mortgage'  => [
                    'name'          => 'Новая программа',
                    'min_deposit'   => 1000000,
                    'interest_rate' => 5.49,
                    'term'          => 30,
                    'validity'      => '2022-12-30'
                ]
            ],
            [
                'apartment' => [
                    'video'  => 'https://www.youtube.com/watch?v=FmCH6blxvP8',
                    'layout' => 'images/uploads/apartment5.png',
                    'type'   => '3 комнатная (евро)',
                    'area'   => 75.6,
                    'price'  => 7500000,
                ],
                'mortgage'  => [
                    'name'          => 'Новая программа',
                    'min_deposit'   => 1000000,
                    'interest_rate' => 5.49,
                    'term'          => 30,
                    'validity'      => '2022-12-30'
                ]
            ],
        ];

        foreach ($aData as $aItem) {
            $iID = Apartment::factory()
                ->state($aItem['apartment'])
                ->create()->getAttribute('id');

            $aItem['mortgage']['apartment'] = $iID;
            Mortgage::factory()
                ->state($aItem['mortgage'])
                ->create();
        }
    }
}
