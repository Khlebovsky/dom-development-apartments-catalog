<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1')->group(function () {
    Route::apiResources([
        '/apartments' => Controllers\ApartmentController::class,
        '/requests'   => Controllers\RequestController::class
    ]);

    Route::prefix('/calculate')->group(function () {
        Route::get('/monthly-payment', [Controllers\CalculatorController::class, 'monthlyPayment']);
    });
});
