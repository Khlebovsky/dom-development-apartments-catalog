#!/bin/sh

docker-compose up -d

cd src

docker exec -it domdev_app composer install && npm install

docker exec -it domdev_app npm run prod

docker exec -it domdev_app php artisan key:generate

docker exec -it domdev_app php artisan migrate

docker exec -it domdev_app php artisan db:seed --class=UserSeeder

docker exec -it domdev_app php artisan db:seed --class=ApartmentsMortgagesSeeder
